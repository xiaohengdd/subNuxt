let format  = require('date-fns').format ;

global.aslkfdjlkasfdasdflj202210609 = {};
global.aslkfdjlkasfdasdflj202210609.appenders = {
  out: {type: 'console'},
  allLog: {type: 'file', filename: './log/all.log', keepFileExt: true, maxLogSize: 10485760, backups: 3},
  httpLog: {type: "dateFile", filename: "log/httpAccess.log", pattern: ".yyyy-MM-dd", keepFileExt: true},
  errorLog: {type: 'file', filename: './log/error.log'},
  error: {type: "logLevelFilter", level: "error", appender: 'errorLog'}
}

global.aslkfdjlkasfdasdflj202210609.categories = {
  http: {appenders: ['out', 'httpLog'], level: "debug"},
  default: {appenders: ['out', 'allLog', 'error'], level: 'debug'},
}

module.exports = class LogUtil {

  constructor(logName,autoInit= true) {
    this.logName = logName;
    this.outPrint = true;
    this.logPath = "log/" + logName + ".log"
    if(autoInit){
      this.init()
    }
  }

  setLogPath(logPath) {
    this.logPath = logPath;
  }

  getLog() {
    return require('log4js');
  }

  isOutPrint(outPrint){
    this.outPrint = outPrint;

  }

  init() {
    let hasCategories = false;
    for(let key in global.aslkfdjlkasfdasdflj202210609.appenders){
     // const itemValue = global.aslkfdjlkasfdasdflj202210609.appenders[key];
       if(key==this.logName){
         hasCategories = true
       }
    }

    if(!hasCategories){
      global.aslkfdjlkasfdasdflj202210609.appenders[this.logName] =  {type: "dateFile", filename: this.logPath, pattern: ".yyyy-MM-dd", keepFileExt: true};
      if(this.outPrint){
        global.aslkfdjlkasfdasdflj202210609.categories[this.logName] =  {appenders: ['out', this.logName], level: 'debug'};
      }else{
        global.aslkfdjlkasfdasdflj202210609.categories[this.logName] =  {appenders: [this.logName], level: 'debug'};
      }
    }


    const config = {};
    config.appenders = global.aslkfdjlkasfdasdflj202210609.appenders
    config.categories = global.aslkfdjlkasfdasdflj202210609.categories

    config.pm2 = true;

    const log4js = this.getLog();
    console.info("初始化了", this.logPath)
    log4js.configure(config);

    this.loggerInfo = log4js.getLogger(this.logName);
    this.loggerError = log4js.getLogger(this.logName);
    return this;
  }

  /**
   * close local console
   */
  closeOutConsole = () =>{
    global.aslkfdjlkasfdasdflj202210609.categories[this.logName] =  {appenders: [this.logName], level: 'debug'};
    this.init()
  }

  //@Deprecated
  ErrorConsole = (err) => {
    this.loggerError.error(format(new Date(), 'yyyy-MM-dd HH:mm:ss') + "   " + err)
   // console.error(dateTime() + ":", err)
  }
  error = (err) => {
    this.ErrorConsole(err)
  }

  /*console info*/
  //@Deprecated
  InfoConsole = (msg) => {
    this.loggerInfo.info(format(new Date(), 'yyyy-MM-dd HH:mm:ss')  + "   " + msg)
  //  console.info(dateTime() + ":", msg)
  }

  info = (msg) => {
    this.InfoConsole(msg)
  }

  NormalConsole = function (msg) {
   console.info(format(new Date(), 'yyyy-MM-dd HH:mm:ss')  + ":", msg)
  }

}
