import ConfirmDialog from './ConfirmDialog.vue'


function install(Vue,options){
  const vuetify = options.app.vuetify
  const ConfirmComponents = Vue.extend(Object.assign({ vuetify }, ConfirmDialog))
  let init = function (data,component) {
    let instance = new ConfirmComponents({
      data,
      component
    }).$mount()
    document.body.appendChild(instance.$el)
    Vue.nextTick(() => {
      instance.show = true
    })
    return instance;
  }

  Vue.prototype.$confirm = init;
}


export default install
